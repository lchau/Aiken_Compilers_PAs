(*
 *  Author: Larry Chau <LarryMChau@gmail.com>
 *  CoolC Stack Implementation for PA1.
 *  Part of Alex Aiken's course on compilers. 
 *  http://openclassroom.stanford.edu/MainFolder/DocumentPage.php?course=Compilers&doc=docs/pa.html
 *)

class Stack {
    stackPtr : SNode;

    push(str : String) : SELF_TYPE {
        let tempPtr : SNode <- (new SNode).init(str, stackPtr)
        in {
            stackPtr <- tempPtr;
            self;
        }
    };

    peek() : String {
        if (isvoid stackPtr) then
            ""
        else
            stackPtr.getData()
        fi
    };

    pop() : String {
        if (isvoid stackPtr) then
            ""
        else
            let str: String <- stackPtr.getData() in {
                stackPtr <- stackPtr.getNext();
                str;
            }
        fi
    };

    print() : SELF_TYPE {
        let tempPtr : SNode <- stackPtr,
            io      : IO    <- new IO
        in {
            while (not (isvoid tempPtr)) loop {
                io.out_string(tempPtr.getData().concat("\n"));
                tempPtr <- tempPtr.getNext();
            } pool;
            self;
        }
    };
};

class SNode {
    data : String;
    next : SNode;

    init(str: String, nextNode : SNode) : SELF_TYPE {
        {
            data <- str;
            next <- nextNode;
            self;
        }
    };

    getNext() : SNode {
        next
    };

    getData() : String {
        data
    };
};

class Main inherits A2I {
   main() : Int {
        (new StackMachine).init()
   };

};
