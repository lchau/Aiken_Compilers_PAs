(*  Author: Larry Chau <LarryMChau@gmail.com>
 *  Stack Machine class which implements excecution logic. 
 *)


class StackMachine inherits A2I {
    stack : Stack;
    io    : IO;

    init() : Int {
        let input : String <- "",
            stop  : Bool   <- false
        in {
            stack <- new Stack;
            io    <- new IO;
            while (not(stop)) loop {
                input <- readInput();
                stop  <- parse(input);
            } pool;
            0;
        }
    };

    readInput() : String {
        let input : String <- ""
        in {
            io.out_string(">");
            input <- io.in_string();
            --io.out_string(input.concat("\n"));
            input;
        }
    };

    parse(command : String) : Bool {
        if (command = "x") then true else {
                if (command = "e") then evaluate(command) else
                if (command = "d") then display(command)  else
                    stack.push(command)
                fi fi;
                false;
            }
        fi
    };

    evaluate(command : String) : SELF_TYPE {
        let topCmd : String <- stack.peek() in {
            if (topCmd = "+") then {
                stack.pop();
                add();
            } else
            if (topCmd = "s") then {
                stack.pop();
                swap(); 
            } else
                self -- do nothing
            fi fi;
        }
    };

    display(command : String) : SELF_TYPE {
        {
            stack.print();
            self;
        }
    };

    add() : SELF_TYPE {
        let x : Int <- a2i(stack.pop()),
            y : Int <- a2i(stack.pop())
        in {    
            stack.push(i2a(x+y));
            self;
        }
    };

    swap() : SELF_TYPE {
        let x : String <- stack.pop(),
            y : String <- stack.pop()
        in {
            stack.push(x);
            stack.push(y);
            self;
        }
    };
};
